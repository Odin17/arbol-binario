package ufps.util;
public class ExceptionUFPS extends Exception{
    
    /**
     * Constructor con parametros de la clase
     * @param mensajeDeError es de tipo String y contiene el mensaje de error para mostrar
     */
    public ExceptionUFPS(String mensajeDeError) {
        
        super(mensajeDeError);
        
    }
    
}//Fin de la Clase
